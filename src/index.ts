import { HDTNResult } from './models/hdtn-result.type';
import { AppConfig } from './models/app-config.interface';
import { Method } from './models/method.enum';
import configService from './services/config.service';
import highlyDivisibleTriangularNumberService from './services/highly-divisible-triangular-number.service';
import outputService from './services/output.service';
import storageService from './services/storage.service';

function defineResultByMethod(config: AppConfig | undefined): HDTNResult | undefined {
  switch (config?.method) {
    case Method.Ordinal:
      return highlyDivisibleTriangularNumberService.sumAndDivisorsByOrdinal(config.value);
    case Method.Divisor:
      return highlyDivisibleTriangularNumberService.sumAndDivisorsByMinDivisors(config.value);
    default:
      console.log('Invalid method given!');
      return undefined;
  }
}

async function main(): Promise<void> {
  try {
    const config = await configService.askAppConfigurations();
    const result = defineResultByMethod(config);
    outputService.printResult(config?.method, result);
    storageService.saveToFile(config, result);
  } catch (error) {
    console.error('Something went wrong!');
    console.error(error);
  }
}

main();
