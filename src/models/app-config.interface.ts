import { Method } from './method.enum';

export interface AppConfig {
  method: Method;
  value: number;
}
