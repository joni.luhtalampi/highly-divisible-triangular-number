import { Method } from './../models/method.enum';
import prompts from 'prompts';
import { AppConfig } from '../models/app-config.interface';

async function askAppConfigurations(): Promise<AppConfig | undefined> {
  const questions: prompts.PromptObject[] = [
    {
      type: 'select',
      name: 'method',
      message: 'Please choose a method:',
      choices: [
        {
          title: 'Ordinal ',
          description: 'The sum and divisors of Xth term',
          value: Method.Ordinal,
        },
        {
          title: 'Divisors',
          description:
            'The first triangle number that have equal to or more than X number of divisors',
          value: Method.Divisor,
        },
      ],
    },
    {
      type: (prev) => (prev === Method.Ordinal ? 'number' : null),
      name: 'value',
      message: 'Please enter an ordinal?',
      validate: (value) =>
        value < 0 || value > 1e9 ? 'Value should be between 1 and 1 000 000 000' : true,
    },
    {
      type: (prev) => (prev === Method.Divisor ? 'number' : null),
      name: 'value',
      message: 'Please enter a minimum number of divisors triangle number should have?',
      validate: (value) =>
        value < 0 || value > 1e4 ? 'Value should be between 1 and 10 000' : true,
    },
  ];
  try {
    const responses = await prompts(questions);
    return Object.keys(responses).length > 0 ? (responses as AppConfig) : undefined;
  } catch (error) {
    console.error('Cannot define app configurations');
    console.error(error);
    return undefined;
  }
}

export default { askAppConfigurations };
