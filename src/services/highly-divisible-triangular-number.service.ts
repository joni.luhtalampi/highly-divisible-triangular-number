import { HDTNResult } from './../models/hdtn-result.type';

function sumOfNaturalNumbers(n: number): number {
  return (n * (n + 1)) / 2;
}

function defineDivisors(n: number): number[] {
  const smallDivisors = [];
  const bigDivisors = [];
  for (let i = 0; i < Math.sqrt(n); i++) {
    if (n % i === 0) {
      smallDivisors.push(i);
      bigDivisors.push(n / i);
    }
  }
  if (Math.sqrt(n) % 1 === 0) {
    smallDivisors.push(Math.sqrt(n));
  }
  bigDivisors.reverse();
  return [...smallDivisors, ...bigDivisors];
}

function sumAndDivisorsByOrdinal(ordinal: number): HDTNResult {
  const term = sumOfNaturalNumbers(ordinal);
  return [term, defineDivisors(term)];
}

function sumAndDivisorsByMinDivisors(minDivisors: number): HDTNResult {
  let term = 0;
  let divisors: number[] = [];
  for (let i = 0; i < 100000; i++) {
    term += i;
    divisors = defineDivisors(term);
    if (divisors.length >= minDivisors) {
      break;
    }
  }
  return [term, divisors];
}

export default { sumAndDivisorsByOrdinal, sumAndDivisorsByMinDivisors };
