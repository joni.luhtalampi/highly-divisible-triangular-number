import { HDTNResult } from './../models/hdtn-result.type';
import { Method } from './../models/method.enum';

function generateOutput(result: HDTNResult | undefined): string {
  if (result) {
    return `${result[0].toString()}: ${result[1].join(',')}`;
  } else {
    return '';
  }
}

function printResult(method: Method | undefined, result: HDTNResult | undefined): void {
  const output = generateOutput(result);
  switch (method) {
    case Method.Ordinal:
      console.log(output);
      break;
    case Method.Divisor:
      const [term, divisors] = output.split(': ');
      console.log(`The triangle number is ${term} and divisors are ${divisors}`);
      break;
    default:
      break;
  }
}

export default { generateOutput, printResult };
