import { AppConfig } from './../models/app-config.interface';
import { Method } from './../models/method.enum';
import { writeFileSync } from 'fs';
import { HDTNResult } from './../models/hdtn-result.type';
import outputService from './output.service';
import path from 'path';

function defineFileName(config: AppConfig): string {
  switch (config.method) {
    case Method.Ordinal:
      return `Divisors and sum of ${config.value}th term.txt`;
    case Method.Divisor:
      return `The first triangle number ${config.value}.txt`;
    default:
      return '';
  }
}

function saveToFile(config: AppConfig | undefined, result: HDTNResult | undefined): void {
  if (config && result) {
    const output = outputService.generateOutput(result);
    const fileName = defineFileName(config);
    const filePath = path.join(__dirname, `../../${fileName}`);
    writeFileSync(filePath, output);
  }
}

export default { saveToFile };
