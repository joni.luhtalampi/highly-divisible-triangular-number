# Highly Divisible Triangular Number

## Instructions

- installation

  ```sh
  yarn install
  ```

- start

  ```sh
  yarn start
  ```

- build

  ```sh
  yarn build
  ```

- dev

  ```sh
    yarn dev
  ```
